// tpng - Unified TP/sit/jumpy around script
//
// v1 - Initial combination of multiple script modes
// v2 - Remove owner-only stuff on menu
// v3 - Finish MODE_CMERE when MODE_MULTI has no targets
// v4 - Fix PAIR mode bug where 'collecting' is skipped
// v5 - Use llSetRegionPos() to get around ban lines
// v6 - Fix rotation at destination
// v7 - Add target handling
//
// Uses WaS handler_blocking_fast method
//
// Configure in object desciption:
// <src>:[<dest>][:channel=<channel>][:alpha=n][:nohover][:debug]
//
// The avatar's final rotation is set by the rotation of this prim

// Set FALSE for no hover text
integer hover_text = TRUE;

// Set to default net channel
integer default_channel = -424242;

// Set to the 'normal' transpareny value for not-hidden state
// Set to 0.0 to stay hidden
float alpha_on = 1.0;

// Set the static text for multi mode
string multi_hover_text = "Pip";

// Set an offset delta for Z at destination
float dest_z_delta = 0.0;

// Notification to target and links that TP was triggered
// Set to 0 to disable
integer trigger_channel = 0;

// Set to FALSE if you dont want the script to say anything while 'working'
// Set to >1 to increase verbosity
// 0 = quiet    (FALSE)
// 1 = minimal  (TRUE)
// 2 = warning
// 3 = debug
integer chatty = 1;

/// ----------

// Operational mode constants

// Default startup
integer MODE_NONE   =  0;

// Target mode is a pair object, no sit, used to provide a location for another TP
integer MODE_TARGET =  1;

// C'Mere mode is a single object, sit and immediate stand
integer MODE_CMERE  =  2;

// Pair mode is a traditional symmetric pair of sit and immediate transport
integer MODE_PAIR   =  4;

// Dance mode is a pair object, sit but no stand at target location, return command via chat
// Usually paired with a MODE_TARGET object
// NOT IMPLEMENTED YET
integer MODE_DANCE  =  8;

// Multi mode is a traditional menu-select destination of a group of objects
// - if only one other object is known, pair mode is implied and no menu is displayed
integer MODE_MULTI  = 16;


//                  Target  C'Mere  Pair    Dance   Multi

// Destination      'target' 'none' config  config  implied

// Sit              no      yes     yes     yes     yes

// Listens          yes     no      yes     yes     yes

// Alpha            0.0     0.0     1.0     1.0     1.0

// Mode selection
// MODE_NONE    if <dest> is configured, set MODE_PAIR and listen
//              if <dest> is 'target', set MODE_TARGET, listen for pings
//              if <dest> is 'none', set MODE_CMERE, no listen
//              if no <dest> is configured, listen
//
// MODE_TARGET  turn off sit, listen for pings
//
// MODE_CMERE   turn on sit, do not listen
//
// MODE_PAIR    listen, only save configured <dest>
//
// MODE_DANCE   MODE_PAIR + do not sit at target, play animation, listen for return in char
//
// MODE_MULTI   listen
//              if no dest found, do MODE_CMERE
//              if 1 dest found, do MODE_PAIR
//              else show menu


integer op_mode = 0;
string source = "";
string destination = "";
integer channel;

list pads;

string pad_name;
integer menu_channel;
integer menu_handle = 0;
string anim = "";

integer collecting = FALSE;
vector src_pos = ZERO_VECTOR;
vector dest_pos = ZERO_VECTOR;
rotation dest_rot = ZERO_ROTATION;
key dest_target = NULL_KEY;

log(string msg) {
    if (chatty >= 1 && msg != "") {
        llOwnerSay(msg);
    }
}

warning(string msg) {
    if (chatty >= 2) {
        log(msg);
    }
}

debug(string msg) {
    if (chatty >= 3) {
        log(msg);
    }
}

// parse_args() sets global config directly
// <src>:[<dest>][:channel=<channel>][:alpha=n][:nohover][:debug]

parse_args(string desc) {
        list _args = llParseStringKeepNulls(desc, [ ":" ], []);
        source = llList2String(_args, 0);
        destination = llList2String(_args, 1);

        channel = default_channel;

        integer i = 2;
        string s = "";
        s = llList2String(_args, i);
        while (s != "") {
            debug("args: " + s);

            // Look for key=value args
            list kv = llParseString2List(s, ["="], []);
            string _key = llList2String(kv, 0);
            string _val = llList2String(kv, 1);

            if (_key == "channel") {
                channel = (integer)_val;
                if (channel > 0)
                    channel = -channel;
            }
            if (_key == "alpha") {
                alpha_on = (float)_val;
            }
            else if (_key == "nohover") {
                hover_text = FALSE;
            }
            else if (s == "debug") {
                if (_val == "") {
                    chatty = 3;
                } else {
                    chatty = (integer)_val;
                }
            }
            i++;
            s = llList2String(_args, i);
        }
}

bcast() {
    debug("bcast("+(string) llGetPos()+")");
    llRegionSay(channel, llDumpList2String([source, llGetPos(), llGetRot(), llGetKey()], ":"));
}

// Configure sit target for destination
set_dest(string dest) {
    warning("set_dest("+dest+")");
    src_pos = llGetPos();
    integer index = llListFindList(pads, [dest]);
    if (index > -1) {
        dest_pos = llList2Vector(pads, index+1);
        dest_rot = llList2Rot(pads, index+2);
        dest_target = llList2String(pads, index+3);

        // Adjust for height delta
        dest_pos.z += dest_z_delta;

        llSitTarget(<0,.0,1>, ZERO_ROTATION);
        llSetClickAction(CLICK_ACTION_SIT);
        debug("  set dest_pos=" + (string)dest_pos);
    } else {
        // Set our own pos for safety
        dest_pos = src_pos;
        debug("  set_dest() failed");
    }
}

set_text() {
    llSetSitText("Teleport");
    if (hover_text) {
        if (op_mode == MODE_TARGET) {
            llSetText("Target " + source, <1.0,1.0,1.0>, 1.0);
        } else if (destination != "") {
            llSetText("Teleport to " + destination, <1.0,1.0,1.0>, 1.0);
        } else {
            llSetText(multi_hover_text, <1.0,1.0,1.0>, 1.0);
        }
    } else {
        llSetText("", <1.0,1.0,1.0>, 1.0);
    }
}

show_target_dialog(key user, list targets) {
    list target_names = llList2ListStrided(targets, 0, -1, 3); // extract names from targets
    llDialog(user, "Pick a destination", target_names, menu_channel);
}

//////////////////////////////////////////////////////////
// (c) Wizardry and Steamworks - 2012, License GPLv3    //
// Please see: http://www.gnu.org/licenses/gpl.html     //
// for legal details, rights of fair usage and          //
// the disclaimer and warranty conditions.              //
//////////////////////////////////////////////////////////

// Calculate next jump gate
vector nextJump(vector iPos, vector dPos, integer jumpDistance) {
    // We move 1/jumpDistance from the initial position
    // towards the final destination in the description.
    float dist = llVecDist(iPos, dPos);
    if(dist > jumpDistance)
        return iPos + jumpDistance * (dPos-iPos) / dist;
    return nextJump(iPos, dPos, --jumpDistance);
}

default {
    on_rez(integer start_param) {
        warning("*default on_rez("+(string)start_param+")");
        // Pause just a bit before attempting to find neighbors
        // Useful when rezzing a bunch of TPs all at once
        llSleep(3.0);
        llResetScript();
    }

    state_entry() {
        // Get configuration
        parse_args(llGetObjectDesc());
        warning("*default state_entry()");
        llSitTarget(<0,.0,1>, ZERO_ROTATION);
        llSetClickAction(CLICK_ACTION_SIT);
        if (destination == "target") {
            // MODE_TARGET configured
            op_mode = MODE_TARGET;
            llSetClickAction(CLICK_ACTION_NONE);
        } else if (destination == "none") {
            // MODE_CMERE configured
            op_mode = MODE_CMERE;
        } else if (destination != "") {
            // MODE_PAIR when <dest> is configured
            op_mode = MODE_PAIR;
        } else {
            // Assume multi-mode
            op_mode = MODE_MULTI;
        }
        debug("Init to mode " + (string)op_mode);

        menu_channel = -1 - (integer)("0x" + llGetSubString( (string)llGetKey(), -7, -1) );

        // See who is out there
        if (!(op_mode & (MODE_TARGET | MODE_CMERE))) {
            collecting = TRUE;
        }

        set_text();
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        anim = llGetInventoryName(INVENTORY_ANIMATION, 0);
        state idle;
    }
}

state idle {
    on_rez(integer start_param) {
        warning("*idle on_rez("+(string)start_param+")");
        llSleep(0.5 + llFrand(0.3));
        llResetScript();
    }

    state_entry() {
        warning("*idle state_entry()");
        if (collecting) {
            debug("  collecting: send ping, listen on "+(string)channel);
            llListen(channel, "", NULL_KEY, "");
            llRegionSay(channel ,"ping");
            llSetTimerEvent(2.0);
            llSleep(0.2 + llFrand(0.3));
            bcast();
        } else {
            debug("  op_mode="+(string)op_mode);
            if (op_mode & (MODE_TARGET | MODE_PAIR | MODE_DANCE | MODE_MULTI)) {
                debug("  listen on "+(string)channel);
                llListen(channel, "", NULL_KEY, "");
                if (op_mode == MODE_TARGET) {
                    bcast();
                }
            }
        }
    }

    listen(integer chan, string name, key id, string message) {
        warning("listen("+(string)chan+", "+name+", "+(string)id+", "+message+")");

        if (chan == menu_channel) {
            // Handle menu selection, message has choice
            debug("menu select: "+message);
            set_dest(message);
            llListenRemove(menu_handle);
            state move;
        }

        list args = llParseString2List(message,[ ":" ], []);
        string cmd = llList2String(args, 0);

        if (cmd == "ping") {
            debug("!!! ping heard: collecting="+(string)collecting);
            // See who else is out there
            pads = [];
            llSetTimerEvent(2.0);
            collecting = TRUE;
            bcast();
            return;
        }
        else if (cmd == "move") {
            // Move to selected target
            string dest = llList2String(args, 1);
            debug("move: " + dest);
            set_dest(dest);
            state move;
        }
        else if (cmd == "home") {
            // Move to start
            debug("home ");
            state recall;
        }

        // Process received messages
        if (llGetListLength(args) < 3) return;

        // Look for our configured destination
        string logmsg = "";
        string warnmsg = "";
        logmsg = "Found " + cmd + " at " + llList2String(args, 1);
        if (op_mode & (MODE_PAIR | MODE_DANCE) && cmd != destination) {
            logmsg += "...ignoring";
        } else {
            vector vec = (vector)llList2String(args, 1);
            rotation rot = (rotation)llList2String(args, 2);
            key target = (key)llList2String(args, 3);
            integer i = llListFindList(pads, [cmd]);
            if (i > -1) {
                pads = llListReplaceList(pads, [cmd, vec, rot, target], i, i+3);
            } else {
                pads += [cmd, vec, rot, target];
            }

            if (op_mode & (MODE_PAIR | MODE_DANCE) && cmd == destination) {
                logmsg += "...partner";
                warnmsg = "...setting partner at "+(string)vec;
                set_dest(cmd);
            } else {
                warnmsg = "...adding at "+(string)vec;
            }
            llSitTarget(<0,.0,1>, ZERO_ROTATION);
            llSetClickAction(CLICK_ACTION_SIT);
        }
        log(logmsg);
        warning(warnmsg);
    }

    changed(integer change) {
        warning("changed("+(string)change+")");
        if (change & CHANGED_LINK) {
            // See if someone sat down
            key sitter = llAvatarOnSitTarget();
            if (sitter) {
                if (op_mode == MODE_TARGET) {
                    llUnSit(llAvatarOnSitTarget());
                    llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
                } else {
                    llSetLinkAlpha(LINK_SET, 0.0, ALL_SIDES);
                    llSetText("", <1.0,1.0,1.0>, 1.0);
                    // Do all destination selection logic in run_time_permissions() handler
                    llRequestPermissions(sitter, PERMISSION_TRIGGER_ANIMATION);
                }
            } else {
                // Handle stand
                debug("stand");
                llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
                set_text();
            }
        }
        else if (change & CHANGED_INVENTORY) {
            anim = llGetInventoryName(INVENTORY_ANIMATION, 0);
        }
        else if (change & (CHANGED_SHAPE | CHANGED_SCALE | CHANGED_TEXTURE | CHANGED_OWNER | CHANGED_REGION | CHANGED_REGION_START)) {
            // We got resized or moved, reconfigure
            set_dest(destination);
            bcast();
            llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
            debug("reconfigured");
        }
    }

    run_time_permissions(integer perm) {
        warning("run_time_permissions");
        if (perm & PERMISSION_TRIGGER_ANIMATION) {
            if (!op_mode & (MODE_TARGET | MODE_CMERE) && destination != "") {
                debug("config dest");
                // If a destination is configured, use it
                set_dest(destination);
                state move;
            } else if (llGetListLength(pads) == 4) {
                debug("one dest");
                // If we have exactly one dest use it
                set_dest(llList2String(pads, 0));
                state move;
            } else if (llGetListLength(pads) > 4 && destination == "") {
                debug("multi");
                // Multiple TPs present and no destination configured
                llStopAnimation("sit");
                llStartAnimation(anim);
// Skip menu for mover mode
//                key sitter = llAvatarOnSitTarget();
//                // Give the user a choice of destinations
//                menu_handle = llListen(menu_channel, "", sitter, "");
//                show_target_dialog(sitter, pads);
                // Fall through and let listen() handle the state change
            } else {
                debug("else: destination=" + (string)pads);
                // No TPs and no dest so stand
                llStopAnimation("sit");
                llStopAnimation(anim);
                llSleep(0.15);
                llUnSit(llAvatarOnSitTarget());
                llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
            }
        }
    }

    timer() {
        if (collecting) {
            llSetTimerEvent(0.0);
            collecting = FALSE;
            debug("timer(): collecting=FALSE");
            return;
        }
    }

    moving_end() {
        warning("*idle moving_end()");
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        debug("moved to " + (string)llGetPos());
    }

    touch_start(integer num) {
        warning("*idle touch_start("+(string)num+")");
        llResetTime();
    }

    touch_end(integer num) {
        warning("*idle touch_end("+(string)num+")");
        if (llGetTime() > 2.0) {
            // Reposition on long-touch
            debug("~~~~~~~touch_end()");
        }
    }
}

state move {
    state_entry() {
        warning("*move state_entry()"+(string)dest_target);

        // Send effects trigger
        if (trigger_channel != 0) {
            llMessageLinked(LINK_SET, trigger_channel, "on", dest_target);
            llSleep(0.4);
            if (dest_target != NULL_KEY) {
                llRegionSayTo(dest_target, trigger_channel, "on");
            }
        }

        if (!llSetRegionPos(dest_pos)) {
            debug("llSetRegionPos failed");
        }
        llSetLinkPrimitiveParamsFast(LINK_THIS, [
            PRIM_POSITION, dest_pos,
            PRIM_ROTATION, dest_rot
        ]);
//        if (llAvatarOnSitTarget()) {
//            llStopAnimation("sit");
//            llStopAnimation(anim);
//            llSleep(0.15);
//            llUnSit(llAvatarOnSitTarget());
//        }
        // for point-to-point moves go back to idle here
        // state recall;
        state idle;
    }

    moving_end() {
        warning("*move moving_end()");
    }
}

state recall {
    state_entry() {
        warning("*recall state_entry()");
        llSetTimerEvent(1.175494351E-38);
        llSleep(0.15);
        if (!llSetRegionPos(src_pos)) {
            debug("llSetRegionPos failed");
        }
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_POSITION, src_pos]);
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        state idle;
    }

    moving_end() {
        warning("*recall moving_end()");
        // Wait until we're done moving to make visible again
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
    }
}
