lteleport
=========

Teleport scripts of various sorts.  Most here for reference.

tpng.lsl - The One TP To Rule Them All!

The others for reference:

handler_blocking_fast.lsl - Start with Wizardry and Steamworks
    script, modify because $REASONS.  This is the most reliable
    intra-region script so far.

save_teleport.lsl - Wizardry and Steamworks safe_teleport

sit-offset.lsl - An earlier attempt, includes description parsing

telepad.lsl - Nargus Asturias' Telepad for OpenSim


tpng
====

Multiple operation modes are combined into a single script in an attempt
to simplify configuration and operation.  The modes are:

Target - Does not allow sit, only responds to PINGs so can be used as a
destination by other TPs.

C'mere (come here) - Move avatar to position and immediately stand.  Does
not listen to any broadcast announcements from other TPs, does not respond
to PINGs from other TPs.

Pair - When there are exactly two TPs, sitting will immediately transport
to the partner position.  This mode is implied when only two TPs are found
or set explicitly by specifying destinations in each config.

Multi (menu) - Listens for broadcast announcements from other TPs on the same
channel, adding them to an internal list. Sitting on the TP displays a menu
of the known TPs for the user to select.


Configuration
-------------

The object description field is used to configure tpng.  It contains multiple
settings, separated by a colon (':').  Only one of these settings is required
although in most cases some others are desirable.

Each TP node has a name (aka 'source') and a channel that it uses to communicate.
This is the minimum required to build a working network.  Optionally, a
specific destination may be specified to force 'pair' mode, and some settings
to influence the appearance of the TP node are available.

The syntax diagram of the configuration field looks like this:

	<src>[:<dest>][:channel=<channel>][:alpha=n][:nohover][:debug]

Do not be scared by that, here is what it means and below are examples for
each mode that will be helpful.

<src>		The name of this TP node.  It is always required, and must be unique
			within the channel being used.

<dest>		The name of the destination TP node.  Setting this forces
			'pair' mode and causes to TP node to not listen for any other
			nodes on its channel.  Set the destination to 'target' forces
			the node to 'target' mode.

channel=<channel>	Each TP node network must be on a unique channel.  The nodes
					only respond to other nodes from the same owner so the default
					channel is acceptable unless one owner needs multiple networks.
					The default channel is -424242.

alpha=n		The alpha value for the TP object when at rest.  The default is 1.0,
			which is 'fully visible', equivalent to 0 in the Edit dialog
			Transparency box.

nohover		If present, no hover text will be displayed.

debug		If present, a wealth of semi-useful information will spew into
			the owner's local chat as the TP nodes communicate with each other.


Examples
--------

Simple Target

Set up a simple target node named 'red' that can be used as a destination
but not a source:

		red:target

Same thing but turn off hover text:

		red:target:nohover


Pair

Set up a pair of TP nodes names 'upstairs' and 'downstairs' to instantly move
between on a single touch.

	upstairs config
		upstairs:downstairs

	downstairs config
		downstairs:upstairs


Multi/menu

Set up a TP network for multiple locations 'red', 'yellow', and 'blue' on
channel 999.  The red location is a simple target as shown above:

	red config (similar to Target example)
		red:target:channel=999

	yellow config
		yellow:channel=999

	blue config
		blue:channel=999

Add another node 'green' that pairs directly with 'blue':

	green config
		green:blue:channel=999

Complex Example

This setup requires multiple TPs to jump into position, such as moving multiple
performers onto a stage from backstage.  A pair of TPs for each performer is
required so multiple channels will need to be specified.

The backstage TP is set up for pair mode, the stage TP is in target mode.

The dancer is on channel 222, the singer is on channel 333.

	dancer pair
		dancer-stage:target:channel=222
		dancer-backstage:dancer-stage:channel=222

	singer pair
		singer-stage:target:channel=333
		singer-backstage:singer-stage:channel=333
