// lteleport - WaS handler_blocking_fast method
//
// v3 - add sleep()
// v4 - hide object during TP
// v5 - force final positions in transit, config alpha
// v6 - multiple targets!
//
// Configure in object desciption:
// <src>:[<dest>][:channel=<channel>][:alpha=n][:nohover][:debug]

// final rotation is set by this prim

// Set FALSE for no hover text
integer hover_text = TRUE;

// Set to default net channel
integer default_channel = -424242;

// Set to the 'normal' transpareny value for not-hidden state
float alpha_on = 1.0;

string source = "";
string destination = "";
integer channel;
integer DEBUG = FALSE;

list pads;

string pad_name;
integer menu_channel;
integer menu_handle = 0;
string anim;

integer collecting = FALSE;
vector src_pos = ZERO_VECTOR;
vector dest_pos = ZERO_VECTOR;
rotation dest_rot = ZERO_ROTATION;

debug(string msg) {
    if (DEBUG) {
        llOwnerSay(msg);
    }
}

// parse_args() sets global config directly
// <src>:[<dest>][:channel=<channel>][:alpha=n][:nohover][:debug]

parse_args(string desc) {
        list _args = llParseStringKeepNulls(desc, [ ":" ], []);
        source = llList2String(_args, 0);
        destination = llList2String(_args, 1);

        channel = default_channel;

        integer i = 2;
        string s = "";
        s = llList2String(_args, i);
        while (s != "") {
            debug("args: " + s);

            // Look for key=value args
            list kv = llParseString2List(s, ["="], []);
            string _key = llList2String(kv, 0);
            string _val = llList2String(kv, 1);

            if (_key == "channel") {
                channel = (integer)_val;
                if (channel > 0)
                    channel = -channel;
            }
            if (_key == "alpha") {
                alpha_on = (float)_val;
            }
            else if (_key == "nohover") {
                hover_text = FALSE;
            }
            else if (s == "debug") {
                DEBUG = TRUE;
            }
            i++;
            s = llList2String(_args, i);
        }
}

bcast() {
    llRegionSay(channel, llDumpList2String([source, llGetPos(), llGetRot() ], ":"));
}

// Configure sit target for destination
set_dest(string dest) {
    debug("set_dest("+dest+")");
    src_pos = llGetPos();
    integer index = llListFindList(pads, [dest]);
    if (index > -1) {
        dest_pos = llList2Vector(pads, index+1);
        dest_rot = llList2Rot(pads, index+2);

        llSitTarget(<0,.0,1>, ZERO_ROTATION);
        llSetClickAction(CLICK_ACTION_SIT);
        debug("  set dest_pos=" + (string)dest_pos);
    } else {
        debug("  set_dest() failed");
    }
}

show_target_dialog(key user, list targets) {
    list target_names = llList2ListStrided(targets, 0, -1, 3); // extract names from targets
    llDialog(user, "Pick a destination", target_names, menu_channel);
}

//////////////////////////////////////////////////////////
// (c) Wizardry and Steamworks - 2012, License GPLv3    //
// Please see: http://www.gnu.org/licenses/gpl.html     //
// for legal details, rights of fair usage and          //
// the disclaimer and warranty conditions.              //
//////////////////////////////////////////////////////////

// Calculate next jump gate
vector nextJump(vector iPos, vector dPos, integer jumpDistance) {
    // We move 1/jumpDistance from the initial position
    // towards the final destination in the description.
    float dist = llVecDist(iPos, dPos);
    if(dist > jumpDistance)
        return iPos + jumpDistance * (dPos-iPos) / dist;
    return nextJump(iPos, dPos, --jumpDistance);
}

default {
    on_rez(integer start_param) {
        llResetScript();
    }

    state_entry() {
        // Get configuration
        parse_args(llGetObjectDesc());
        menu_channel = -1 - (integer)("0x" + llGetSubString( (string)llGetKey(), -7, -1) );

        // See who is out there
        collecting = TRUE;
        llListen(channel, "", NULL_KEY, "");
        llRegionSay(channel ,"ping");
        llSleep(0.2 + llFrand(0.3));
        llSetTimerEvent(2.0);
        bcast();
        debug("Configured for " + (string) llGetPos());

        llSetSitText("Teleport");
        if (hover_text) {
            llSetText("Teleport to " + destination, <1.0,1.0,1.0>, 1.0);
        } else {
            llSetText("", <1.0,1.0,1.0>, 1.0);
        }
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        anim = llGetInventoryName(INVENTORY_ANIMATION, 0);
    }

    listen(integer chan, string name, key id, string message) {
        // Only listen to our own
        key ok = llGetOwnerKey(id);
        if (ok != llGetOwner()) return;

        if (message == "ping") {
            // See who else is out there
            pads = [];
            llSetTimerEvent(2.0);
            collecting = TRUE;
            bcast();
            return;
        }

        // Process received messages
        list args = llParseString2List(message,[ ":" ], []);
        if (llGetListLength(args) != 3) return;

        // Look for our configured destination
        string name = llList2String(args, 0);
        vector vec = (vector)llList2String(args, 1);
        rotation rot = (rotation)llList2String(args, 2);
        debug("found '" + name + "' at " + (string)vec);
        integer i = llListFindList(pads, [name]);
        if (i > -1) {
            pads = llListReplaceList(pads, [name, vec, rot], i, i+2);
        } else {
            pads += [name, vec, rot];
        }

        if (name == destination) {
            set_dest(name);
        }
    }

    timer() {
        if (collecting) {
            llSetTimerEvent(0.0);
            collecting = FALSE;
            state idle;
        }
    }
}

state idle {
    state_entry() {
        llListen(channel, "", NULL_KEY, "");
    }

    listen(integer chan, string name, key id, string message) {
        if (chan == menu_channel) {
            // Handle menu selection, message has choice
            set_dest(message);
            llListenRemove(menu_handle);
            state move;
        }

        // Only listen to our own
        key ok = llGetOwnerKey(id);
        if (ok != llGetOwner()) return;

        if (message == "ping") {
            // See who else is out there
            pads = [];
            llSetTimerEvent(2.0);
            collecting = TRUE;
            bcast();
            return;
        }

        // Process received messages
        list args = llParseString2List(message,[ ":" ], []);
        if (llGetListLength(args) != 3) return;

        // Look for our configured destination
        string name = llList2String(args, 0);
        debug("setting up " + name);
        vector vec = (vector)llList2String(args, 1);
        rotation rot = (rotation)llList2String(args, 2);
        integer i = llListFindList(pads, [name]);
        if (i > -1) {
            pads = llListReplaceList(pads, [name, vec, rot], i, i+2);
        } else {
            pads += [name, vec, rot];
        }
    }

    changed(integer change) {
        debug("changed()");
        if (change & CHANGED_LINK) {
            // See if someone sat down
            key sitter = llAvatarOnSitTarget();
            if (sitter) {
                // Do all destination selection logic in run_time_permissions() handler
                llRequestPermissions(sitter, PERMISSION_TRIGGER_ANIMATION);
            }
        }
        else if (change & (CHANGED_SHAPE | CHANGED_REGION)) {
            // We got resized or moved, reconfigure
            set_dest(destination);
            bcast();
            llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
            debug("reconfigured");
        }
    }

    run_time_permissions(integer perm) {
        debug("run_time_permissions");
        if (perm & PERMISSION_TRIGGER_ANIMATION) {
            if (destination != "") {
                // If a destination is configured, use it
                set_dest(destination);
                state move;
            } else if (llGetListLength(pads) == 3) {
                // If we have exactly one dest use it
                set_dest(llList2String(pads, 0));
                state move;
            } else if (llGetListLength(pads) > 3 && destination == "") {
                // Multiple TPs present and no destination configured
                llStopAnimation("sit");
                llStartAnimation(anim);
                key sitter = llAvatarOnSitTarget();
                // Give the user a choice of destinations
                menu_handle = llListen(menu_channel, "", sitter, "");
                show_target_dialog(sitter, pads);
                // Fall through and let listen() handle the state change
            }
        }
    }

    timer() {
        if (collecting) {
            llSetTimerEvent(0.0);
            collecting = FALSE;
            return;
        }
    }

    moving_end() {
        set_dest(destination);
        bcast();
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        debug("moved to " + (string)llGetPos());
    }
}

state move {
    state_entry() {
        llSetLinkAlpha(LINK_SET, 0.0, ALL_SIDES);
        do {
            llSetLinkPrimitiveParamsFast(LINK_THIS,
                [PRIM_POSITION, nextJump(llGetPos(), dest_pos, 10)]
            );
        } while(llVecDist(llGetPos(), dest_pos) > 1);
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_POSITION, dest_pos]);
        if (llAvatarOnSitTarget())
            llStopAnimation("sit");
            llStopAnimation(anim);
            llSleep(0.15);
            llUnSit(llAvatarOnSitTarget());
        state recall;
    }
}

state recall {
    state_entry() {
        llSetTimerEvent(1.175494351E-38);
        llSleep(0.15);
        do {
            llSetLinkPrimitiveParamsFast(LINK_THIS,
                [PRIM_POSITION, nextJump(llGetPos(), src_pos, 10)]
            );
        } while(llVecDist(llGetPos(), src_pos) > 1);
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_POSITION, src_pos]);
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        state idle;
    }

    moving_end() {
        // Wait until we're done moving to make visible again
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
    }
}
