// dance position
// v2 - cleanups for general use; add hide, show commands
// v3 - fix set dest bug
//
// lteleport - WaS handler_blocking_fast method
// modified for positioning not teleporting
//
// v3 - add sleep()
// v4 - hide object during TP
// v5 - force final positions in transit, config alpha
// v5p - positioner, adds UpdateSitTarget()
//
// Configure in object desciption:
// <src>:<dest>[:channel=<channel>][:alpha=n][:nohover][:debug]

// final rotation is set by this prim

// Set FALSE for no hover text
integer hover_text = TRUE;

// Listen channel - Set this to the channel to issue chat commands
integer CMD_CHANNEL = 4220;

// Set to default net channel
integer default_channel = -424242;

// Set to the 'normal' transpareny value for not-hidden state
float alpha_on = 1.0;

// Set the starting position and rotation
vector START_POS = <0.0, 0.0, 0.01>;
vector delta_rot = <0.0, 0.0, 0.0>;
rotation final_rot = ZERO_ROTATION;

integer DEBUG = FALSE;

list pads;

string pad_name;
string source;
string destination;
integer channel;

integer collecting = FALSE;
vector src_pos = ZERO_VECTOR;
vector dest_pos = ZERO_VECTOR;
rotation dest_rot = ZERO_ROTATION;

integer rez_bird = FALSE;
string bird_name = "bird";
vector bird_pos = <0.0, 0.0, 0.5>;
vector bird_rot = <0.0, 0.0, 130.0>;  // degrees
integer bird_timer = 20;

debug(string msg) {
    if (DEBUG) {
        llOwnerSay(msg);
    }
}

// parse_args() returns a list of the parsed description: [source, destination, channel]
// <src>:<dest>[:<channel>][:nohover][:debug]
// Additional arguments are set directly and not returned

list parse_args(string desc) {
        list _args = llParseString2List(desc, [ ":" ], []);
        string _src = llList2String(_args, 0);
        string _dest = llList2String(_args, 1);

        integer _chan = (integer)llList2String(_args, 2);
        if (_chan == 0) {
            _chan = default_channel;
        } else {
            if (_chan > 0)
                _chan = - _chan;
        }

        integer i = 2;
        string s = "";
        s = llList2String(_args, i);
        while (s != "") {
            debug("args: " + s);

            // Look for key=value args
            list kv = llParseString2List(s, ["="], []);
            string _key = llList2String(kv, 0);
            string _val = llList2String(kv, 1);

            if (_key == "channel") {
                _chan = (integer)_val;
                if (_chan > 0)
                    _chan = - _chan;
            }
            if (_key == "alpha") {
                alpha_on = (float)_val;
            }
            else if (_key == "nohover") {
                hover_text = FALSE;
            }
            else if (s == "debug") {
                DEBUG = TRUE;
            }
            i++;
            s = llList2String(_args, i);
        }

        return [_src, _dest, _chan];
}

bcast() {
    llRegionSay(channel, llDumpList2String([source, llGetPos(), llGetRot() ], ":"));
}

// Sets / Updates the sit target moving the avatar on it if necessary.
// Written by Strife Onizuka, size adjustment and improvements provided by Talarus Luan
UpdateSitTarget(vector pos, rotation rot) {
    // Using this while the object is moving may give unpredictable results.
    llSitTarget(pos, rot); //Set the sit target
    key user = llAvatarOnSitTarget();
    if (user) {
        // true if there is a user seated on the sittarget, if so update their position
        vector size = llGetAgentSize(user);
        if (size) {
            // The user really exists.
            // We need to make the position and rotation local to the current prim
            rotation localrot = ZERO_ROTATION;
            vector localpos = ZERO_VECTOR;
            if (llGetLinkNumber() > 1) {
                // only need the local rot if it's not the root
                localrot = llGetLocalRot();
                localpos = llGetLocalPos();
            }
            integer linkNum = llGetNumberOfPrims();
            do {
                if (user == llGetLinkKey(linkNum)) {
                    // Make sure the index is valid.
                    // <0.008906, -0.049831, 0.088967> are the coefficients for a parabolic curve that best fits real avatars. It is not a perfect fit.
                    float fAdjust = ((((0.008906 * size.z) + -0.049831) * size.z) + 0.088967) * size.z;
                    llSetLinkPrimitiveParamsFast(
                        linkNum,
                        [
                        PRIM_POS_LOCAL, (pos + <0.0, 0.0, 0.4> - (llRot2Up(rot) * fAdjust)) * localrot + localpos,
                        PRIM_ROT_LOCAL, rot * localrot
                        ]
                    );
                    jump end ;//cheaper but a tad slower then return
                }
            }while(--linkNum);
        }
        else {
            //It is rare that the sit target will bork but it does happen, this can help to fix it.
            llUnSit(user);
        }
    }
    @end;
}

// Configure sit target for destination
set_dest(string dest) {
    debug("set_dest("+dest+")");
    src_pos = llGetPos();
    integer index = llListFindList(pads, [dest]);
    if (index > -1) {
        dest_pos = llList2Vector(pads, index+1);
        dest_rot = llList2Rot(pads, index+2);

        llSitTarget(<0,.0,1>, ZERO_ROTATION);
//        UpdateSitTarget(START_POS, final_rot);
        llSetClickAction(CLICK_ACTION_SIT);
        debug("  set dest_pos=" + (string)dest_pos);
    } else {
        debug("  set_dest() failed");
    }
}

//////////////////////////////////////////////////////////
// (c) Wizardry and Steamworks - 2012, License GPLv3    //
// Please see: http://www.gnu.org/licenses/gpl.html     //
// for legal details, rights of fair usage and          //
// the disclaimer and warranty conditions.              //
//////////////////////////////////////////////////////////

// Calculate next jump gate
vector nextJump(vector iPos, vector dPos, integer jumpDistance) {
    // We move 1/jumpDistance from the initial position
    // towards the final destination in the description.
    float dist = llVecDist(iPos, dPos);
    if(dist > jumpDistance)
        return iPos + jumpDistance * (dPos-iPos) / dist;
    return nextJump(iPos, dPos, --jumpDistance);
}

default {
    on_rez(integer start_param) {
        llResetScript();
    }

    state_entry() {
        // Get configuration
        list desc = parse_args(llGetObjectDesc());
        source = llList2String(desc, 0);
        destination = llList2String(desc, 1);
        channel = llList2Integer(desc, 2);

        // Set up the sit target
        final_rot = llEuler2Rot(delta_rot * DEG_TO_RAD);
        UpdateSitTarget(START_POS, final_rot);

        // See who is out there
        collecting = TRUE;
        llListen(channel, "", NULL_KEY, "");
        llRegionSay(channel ,"ping");
        llSleep(0.2 + llFrand(0.3));
        llSetTimerEvent(2.0);
        bcast();
        llSetSitText("Teleport");
        if (hover_text) {
            llSetText(destination, <1.0,1.0,1.0>, 1.0);
        } else {
            llSetText("", <1.0,1.0,1.0>, 1.0);
        }
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        rez_bird = FALSE;
        debug("Configured for " + (string) llGetPos());
    }

    listen(integer chan, string name, key id, string message) {
        // Only listen to our own
        key ok = llGetOwnerKey(id);
        if (ok != llGetOwner()) return;

        if (message == "ping") {
            // See who else is out there
            pads = [];
            llSetTimerEvent(2.0);
            collecting = TRUE;
            bcast();
            return;
        }

        // Process received messages
        list args = llParseString2List(message,[ ":" ], []);
        if (llGetListLength(args) != 3) return;

        // Look for our configured destination
        string name = llList2String(args, 0);
        if (name != destination)
            return;

        vector vec = (vector)llList2String(args, 1);
        rotation rot = (rotation)llList2String(args, 2);
        debug("found '" + name + "' at " + (string)vec);
        integer i = llListFindList(pads, [name]);
        if (i > -1) {
            pads = llListReplaceList(pads, [name, vec, rot], i, i+2);
        } else {
            pads += [name, vec, rot];
        }

        if (name == destination) {
            set_dest(name);
        }
    }

    changed(integer change) {
        debug("changed()");
        if (change & CHANGED_LINK) {
            // See if someone sat down
            key sitter = llAvatarOnSitTarget() ;
            if (sitter) {
                llRequestPermissions(sitter , PERMISSION_TRIGGER_ANIMATION);
                debug("after req perm");

                // Get agent height to adjust the sit pos
                vector agent = llGetAgentSize(sitter);
                vector sit_here = START_POS;

                // This is for cubes
                sit_here.z += .01 + agent.z / 2;

                // This is for cylinders
//                sit_here.z += (3 + agent.z / 2);

                // This is for (dimpled) spheres
//                sit_here.x -= (.3 + agent.z / 2);

//                UpdateSitTarget(sit_here, final_rot);
//                state move;
            }
        }
        else if (change & (CHANGED_SHAPE | CHANGED_REGION)) {
            // We got resized or moved, reconfigure
            set_dest(destination);
            bcast();
            llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
            debug("reconfigured");
        }
    }

    run_time_permissions(integer perm) {
        debug("run_time_permissions");
        if (perm & PERMISSION_TRIGGER_ANIMATION) {
            llStopAnimation("sit");
            state move;
        }
    }

    timer() {
        if (collecting) {
            llSetTimerEvent(0.0);
            collecting = FALSE;
            return;
        }
    }

    moving_end() {
        set_dest(destination);
        bcast();
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        debug("moved to " + (string)llGetPos());
    }
}

state move {
    state_entry() {
        debug("move:state_entry()");
        // Set up command listener
        llListen(CMD_CHANNEL, "", NULL_KEY, "");

        set_dest(destination);
        llSetLinkAlpha(LINK_SET, 0.0, ALL_SIDES);
        llSetText("", <1.0,1.0,1.0>, 1.0);
        do {
            llSetLinkPrimitiveParamsFast(LINK_THIS,
                [PRIM_POSITION, nextJump(llGetPos(), dest_pos, 10)]
            );
        } while(llVecDist(llGetPos(), dest_pos) > 1);
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_POSITION, dest_pos]);
//        if (llAvatarOnSitTarget())
//            llSleep(0.25);
//            llUnSit(llAvatarOnSitTarget());
//        state recall;
    }

    listen(integer channel, string name, key id, string message) {
        // Parse command
        string cmd = "";
        string args = "";
        integer idx = llSubStringIndex(message, " ");
        if (idx == -1) {
            cmd = message;
            args = "";
        } else {
            cmd = llGetSubString(message, 0, idx-1);
            args = llDeleteSubString(message, 0, idx);
        }
        cmd = llToLower(cmd);
        debug("cmd: " + cmd);

        // Parse a number too?
        integer num = (integer)cmd;

        if (cmd == "reset") {
            llResetScript();
        } else if (cmd == "hide") {
            llSetLinkAlpha(LINK_SET, 0.0, ALL_SIDES);
        } else if (cmd == "show") {
            llSetLinkAlpha(LINK_SET, 1.0, ALL_SIDES);
        } else if (cmd == "start" || cmd == "on") {
//            start_particles();
        } else if (cmd == "stop" || cmd == "off") {
            state recall;
        } else if (cmd == "return") {
            rez_bird = TRUE;
            state recall;
        }
    }

    changed(integer change) {
        if (change & CHANGED_LINK) {
            // See if someone sat down
            if (llAvatarOnSitTarget()) {
                debug("move:changed - sat?");
                state move;
            } else {
                // Stood up
                debug("move:changed - stood?");
                llUnSit(llAvatarOnSitTarget());
                state recall;
            }
        }
    }
}

state recall {
    state_entry() {
        llSetTimerEvent(1.175494351E-38);
        llSleep(0.1);
        if (rez_bird) {
            debug("rez bird");
            llRezObject(
                bird_name,
                llGetPos() + bird_pos,
                <0.0, 0.0, 0.0>,
                llEuler2Rot(bird_rot * DEG_TO_RAD), // <0.0, 0.0, 0.0, 1.0>,
                bird_timer
            );
        }
        do {
            llSetLinkPrimitiveParamsFast(LINK_THIS,
                [PRIM_POSITION, nextJump(llGetPos(), src_pos, 10)]
            );
        } while(llVecDist(llGetPos(), src_pos) > 1);
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_POSITION, src_pos]);

        debug("recall:state_entry() - unsit");
        llStopAnimation("sit");
        llUnSit(llAvatarOnSitTarget());

        // Send to home position one last time because it doesn't always make it
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_POSITION, src_pos]);

        state default;
    }

    moving_end() {
        // may not get called???
        // Wait until we're done moving to make visible again
        llSetLinkAlpha(LINK_SET, alpha_on, ALL_SIDES);
        llSetText(destination, <1.0,1.0,1.0>, 1.0);
    }
}
